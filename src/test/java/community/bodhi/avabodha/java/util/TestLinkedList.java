package community.bodhi.avabodha.java.util;

import static org.junit.Assert.assertThrows;

import java.util.LinkedList;
import java.util.NoSuchElementException;

import org.assertj.core.api.SoftAssertions;

/**
 * 
 * @author shrivallabh@rjds.in
 *
 */
public class TestLinkedList extends TestList<LinkedList<Integer>> {

	@Override
	protected LinkedList<Integer> createFixtureInstance() {
		// TODO Auto-generated method stub
		return new LinkedList<>();
	}

	@Override
    protected String collectionClassName() {
        // TODO Auto-generated method stub
        return "LinkedList";
    }
	
	public void TestAddFirst() {
		getFixture().addFirst(9);
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture())
					.as("The size of fixture has gone up to 3")
					.hasSize(3);
				softly.assertThat(getFixture().get(0))
					.as("The First element is 9")
					.isEqualTo(9);
			});
	}
	
	public void TestAddLast() {
		getFixture().addLast(12);
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture())
					.as("The size of fixture has gone up to 3")
					.hasSize(3);
				softly.assertThat(getFixture().get(2))
					.as("The last element is 12")
					.isEqualTo(12);
			});
	}

	public void TestElement() {
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture().element())
					.as("The first element is 10")
					.isEqualTo(10);
			});
		getFixture().clear();
		assertThrows(NoSuchElementException.class, 
					 ()->getFixture().element());
		
	}

	public void TestGetFirst() {
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture().getFirst())
					.as("The first element is 10")
					.isEqualTo(10);
			});
		getFixture().clear();
		assertThrows(NoSuchElementException.class, 
					 ()->getFixture().getFirst());
		
	}

	public void TestGetLast() {
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture().getFirst())
					.as("The first element is 10")
					.isEqualTo(10);
			});
		getFixture().clear();
		assertThrows(NoSuchElementException.class, 
					 ()->getFixture().getLast());
		
	}
	
	public void TestOffer() {
		getFixture().offer(12);
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture())
					.as("The size of fixture has gone up to 3")
					.hasSize(3);
				softly.assertThat(getFixture().get(2))
					.as("The returned element is 12")
					.isEqualTo(12);
			});
	}

	public void TestOfferFirst() {
		getFixture().offer(9);
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture())
					.as("The size of fixture has gone up to 3")
					.hasSize(3);
				softly.assertThat(getFixture().get(0))
					.as("The returned element is 9")
					.isEqualTo(12);
			});
	}

	public void TestOfferLast() {
		getFixture().offer(12);
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(getFixture())
					.as("The size of fixture has gone up to 3")
					.hasSize(3);
				softly.assertThat(getFixture().get(0))
					.as("The returned element is 12")
					.isEqualTo(12);
			});
	}

	public void TestPeek() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().peek();
				softly.assertThat(getFixture())
					.as("The size of fixture is unchanged")
					.hasSize(2);
				softly.assertThat(element)
					.as("The returned element is 10")
					.isEqualTo(10);
			});
	}

	public void TestPeekFirst() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().peekFirst();
				softly.assertThat(getFixture())
					.as("The size of fixture is unchanged")
					.hasSize(2);
				softly.assertThat(element)
					.as("The returned element is 10")
					.isEqualTo(10);
			});
	}

	public void TestPeekLast() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().peekLast();
				softly.assertThat(getFixture())
					.as("The size of fixture is unchanged")
					.hasSize(3);
				softly.assertThat(element)
					.as("The returned element is 11")
					.isEqualTo(11);
			});
	}

	public void TestPoll() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().poll();
				softly.assertThat(getFixture())
					.as("The size of fixture is reduced by 1")
					.hasSize(1);
				softly.assertThat(element)
					.as("The returned element is 10")
					.isEqualTo(10);
			});
	}

	public void TestPollFirst() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().peekFirst();
				softly.assertThat(getFixture())
					.as("The size of fixture is reduced by 1")
					.hasSize(1);
				softly.assertThat(element)
					.as("The returned element is 10")
					.isEqualTo(10);
			});
	}

	public void TestPollLast() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().peekLast();
				softly.assertThat(getFixture())
					.as("The size of fixture is reduced by 1")
					.hasSize(1);
				softly.assertThat(element)
					.as("The returned element is 11")
					.isEqualTo(11);
			});
	}
	
	public void TestPop() {
		SoftAssertions.assertSoftly(
			softly-> {
				Integer element = getFixture().pop();
				softly.assertThat(getFixture())
					.as("The size of fixture is reduced by 1")
					.hasSize(1);
				softly.assertThat(element)
					.as("The returned element is 10")
					.isEqualTo(10);
			});
	}

	public void TestPush() {
		SoftAssertions.assertSoftly(
			softly-> {
				getFixture().push(9);
				softly.assertThat(getFixture())
					.as("The size of fixture is increased by 1")
					.hasSize(3);
				softly.assertThat(getFixture().get(0))
					.as("The first element is 9")
					.isEqualTo(9);
			});
	}
/*
 * TODO:
	Iterator<E>  descendingIterator​() 
	E  removeFirst​() 
	boolean  removeFirstOccurrence​(Object o) 
	E  removeLast​() 
	boolean  removeLastOccurrence​(Object o)
 *  
 */
}
