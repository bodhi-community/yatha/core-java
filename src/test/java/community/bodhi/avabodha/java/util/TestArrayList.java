package community.bodhi.avabodha.java.util;

/**
 * @author shrivallabh@rjds.in
 */
import java.util.ArrayList;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;
public class TestArrayList extends TestList<ArrayList<Integer>> {

	@Override
	protected ArrayList<Integer> createFixtureInstance() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
    protected String collectionClassName() {
        // TODO Auto-generated method stub
        return "ArrayList";
    }

    @Test
	public void testRemoveIf​() {
    	// remove all the even items
		getFixture().removeIf(item->item%2==0);
		assertThat(getFixture())
			.as("Should contain only items that are odd")
			.allMatch(item->item%2!=0);
	}
}
