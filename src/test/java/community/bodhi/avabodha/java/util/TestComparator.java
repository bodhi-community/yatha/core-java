package community.bodhi.avabodha.java.util;

import static java.util.Collections.sort;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.Comparator.nullsLast;

import java.util.Comparator;
import java.util.LinkedList;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author shrivallabh@rjds.in
 * 
 */

public class TestComparator {
	
	private LinkedList<Employee> fixture;
	private Employee aditya;
	private Employee sumedh;
	private Employee omkar;
	private Employee shrivallabh;
	
	@Before
	public void setup() {
		fixture=new LinkedList<>();
		shrivallabh = new Employee(1L, "Shrivallabh Deshmukh", "Founder", 37, 40000D, new Address("Nashik", "Maharashtra"));
		fixture.add(shrivallabh);
		omkar = new Employee(2L, "Omkar Harkare", "Initiate", 20, 30000D, new Address("Pune", "Maharashtra"));
		fixture.add(omkar);
		sumedh = new Employee(3L, "Sumedh Deshpande", "Initiate", 20, 20000D, new Address("Pune", "Maharashtra"));
		fixture.add(sumedh);
		aditya = new Employee(4L, "Aditya", "Initiate", 20, 10000D, new Address("Pune", "Maharashtra"));
		fixture.add(aditya);
	}

	@Test
	public void testComparing() {
		// Sort by name
		sort(fixture, comparing(Employee::getName));
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getFirst())
					.as("Lexicographically Aditya should be the first entry")
					.isEqualTo(aditya);
				softly.assertThat(fixture.getLast())
					.as("Lexicographically Sumedh should be the last entry")
					.isEqualTo(sumedh);
			});
	}

	@Test
	public void testReversed() {
		// Sort descending by name
		sort(fixture, comparing(Employee::getId).reversed());
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getFirst())
					.as("By descending ids Aditya should be the first entry")
					.isEqualTo(aditya);
				softly.assertThat(fixture.getLast())
					.as("By descending ids Shrivallabh should be the last entry")
					.isEqualTo(shrivallabh);
			});		
	}
	
	@Test
	public void testComparingWithKeyComparator() {
		// Sort by name
		sort(fixture, comparing(Employee::getName, Comparator.reverseOrder()));
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getFirst())
					.as("In Lexicographical descending order, Sumedh should be the "
							+ "first entry")
					.isEqualTo(sumedh);
				softly.assertThat(fixture.getLast())
					.as("In Lexicographical descending order, Aditya should be the "
							+ "last entry")
					.isEqualTo(aditya);
			});
		
	}
	
	@Test
	public void testNullsFirst() {
		
		// Adding null entry. 
		fixture.add(null);
		// Sort by age
		sort(fixture, nullsFirst(comparing(Employee::getAge)));
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getFirst())
					.as("Deepak should be the first entry")
					.isEqualTo(null);
			});
		
	}

	@Test
	public void testNullsLast() {
		
		// Adding null entry. 
		fixture.add(null);
		// Sort by age
		sort(fixture, nullsLast(comparing(Employee::getAge)));
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getLast())
					.as("Deepak should be the last entry")
					.isEqualTo(null);
			});
		
	}

	@Test
	public void testThenComparing() {
		
		// Adding entry with null age. 
		// Sort by age
		sort(fixture, comparing(Employee::getAge)
						.thenComparing(Employee::getSalary));
		SoftAssertions.assertSoftly(
			softly-> {
				softly.assertThat(fixture.getFirst())
					.as("Aditya should be the first entry due to salary")
					.isEqualTo(aditya);
				softly.assertThat(fixture.getLast())
					.as("Shrivallabh should be the last entry as he has maximum"
							+ " age")
					.isEqualTo(shrivallabh);
			});
		
	}
	
	@Test
	public void testThenComparingOther() {
		Comparator<Employee> comp = Comparator.comparing(Employee::getAge);
		
		sort(fixture, comparing(Employee::getName)
						.thenComparing(comp));
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getFirst())
						.as("Aditya should be the first entry due to age")
						.isEqualTo(aditya);
				});
		
	}
	
	@Test
	public void testThenComparingWithKeyComparator() {
		
		sort(fixture, comparing(Employee :: getName).thenComparing(Employee::getAddress, 
							(address1,address2) -> address1.getCity().compareTo(address2.getCity())));
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getFirst())
						.as("Aditya will be first entry due to name")
						.isEqualTo(aditya);
					softly.assertThat(fixture.getLast())
						.as("Sumedh will be last entry")
						.isEqualTo(sumedh);
				});
	}
	
	@Test
	public void testThenComparingDouble() {
		
		sort(fixture, comparing(Employee::getAge)
				.thenComparingDouble(Employee::getSalary));
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getFirst())
						.as("Aditya will be first entry due to Salary")
						.isEqualTo(aditya);
					softly.assertThat(fixture.get(2))
						.as("Omkar will be third entry due to Salary")
						.isEqualTo(omkar);
			
		});
	}
	
	@Test
	public void testThenComparingInt() {
		sort(fixture, comparing(Employee::getName)
				.thenComparingInt(Employee::getAge));
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getFirst())
						.as("Aditya will be first entry due to name")
						.isEqualTo(aditya);
					softly.assertThat(fixture.getLast())
						.as("Shrivallabh will be last entry due to name")
						.isEqualTo(sumedh);
			
		});
	}
	
	@Test
	public void testComparator() {
		sort(fixture, new SortByAge());
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getLast())
						.as("Shrivallabh will be last entry as per age")
						.isEqualTo(shrivallabh);
					softly.assertThat(fixture.getFirst())
						.as("Omkar will be first entry")
						.isEqualTo(omkar);
			
		});
	}
	
	@Test 
	public void testComparable() {
		sort(fixture);
		SoftAssertions.assertSoftly(
				softly -> {
					softly.assertThat(fixture.getFirst())
						.as("Shrivallabh will be first entry as per Salary")
						.isEqualTo(shrivallabh);
					softly.assertThat(fixture.getLast())
						.as("Sumedh will be last entry")
						.isEqualTo(aditya);
			
		});
	}
	
}

/**
 * Instances of these classes are compared during tests
 * @author shriv
 *
 */
class Employee implements Comparable<Employee> {
	
	private Long id;
	private String name;
	private String designation;
	private Integer  age;
	private Double salary;
	Address address;
	
	public Employee() {
		
	}

	
	public Employee(Long id, String name, String designation, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.designation = designation;
		this.age = age;
	}
	
	public Employee(Long id, String name, String designation, Integer age, Double salary, Address address) {
		this(id, name, designation, age);
		this.salary = salary;
		this.address = address;
	}	

	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}


	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDesignation() {
		return designation;
	}
	
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Double getSalary() {
		return salary;
	}


	public void setSalary(Double salary) {
		this.salary = salary;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((designation == null) ? 0 : designation.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((salary == null) ? 0 : salary.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (designation == null) {
			if (other.designation != null)
				return false;
		} else if (!designation.equals(other.designation))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (salary == null) {
			if (other.salary != null)
				return false;
		} else if (!salary.equals(other.salary))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", designation=" + designation 
				+ ", age=" + age + ", salary=" + salary + "]";
	}


	@Override
	public int compareTo(Employee e1) {
		if(this.getSalary() > e1.getSalary()) 
			return 1 ;
		
		return 0;
	}


}

class Address {
	private String city;
	private String state;

	public Address(String city, String state) {
		this.city = city;
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}

class SortByAge implements Comparator<Employee> {
	public int compare(Employee a, Employee b) {
		return a.getAge() - b.getAge();
	}
}
