package community.bodhi.avabodha.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

/**
 * Bunch of tests to test the behavior of List interface.
 * The fixture is instantiated in the concrete implementation of the test class. 
 * 
 * @author shrivallabh@rjds.in
 *
 */
public abstract class TestList<T extends List<Integer>> extends TestCollection<T> {

	
	@Test 
    public void testGet() {
	    assertThat(getFixture().get(0))
	        .as("The first element in the list is 10")
	        .isEqualTo(10);
	    
	    assertThatExceptionOfType(IndexOutOfBoundsException.class)
	        .as("OOBE index is thrown when index larger than size of array is specified "
	                + "for get operation")
	        .isThrownBy(() -> getFixture().get(10));
        assertThatExceptionOfType(IndexOutOfBoundsException.class)
            .as("OOBE index is thrown when negative index is specified for get operation")
            .isThrownBy(() -> getFixture().get(-1));
	    
    }
	
	@Test 
	public void testAddAtIndex() {
		// Add at second position
		getFixture().add(1, 12);
        SoftAssertions.assertSoftly(
                softly-> {
                    softly.assertThat(getFixture())
                        .as("Size of list is now 3")
                        .hasSize(3);
                    softly.assertThat(getFixture())
                        .as("12 is present in the list")
                        .contains(12);
                    softly.assertThat(getFixture().get(1))
                        .as("12 is present at index 1 of the list")
                        .isEqualTo(12);
            });

		// Add at OOB position
		assertThatExceptionOfType(IndexOutOfBoundsException.class)
		    .as("OOBE is thrown when index larger than size of the list is specified"
		            + " for addAll operation.")
            .isThrownBy(()-> {
                getFixture().add(5, 12);
            });
        assertThatExceptionOfType(IndexOutOfBoundsException.class)
            .as("OOBE index is thrown when negative index is specified for get operation")
            .isThrownBy(() -> getFixture().get(-1));

	}
		
    @Test 
    public void testAddAllAtIndex() {
        List<Integer> listToAdd=Arrays.asList(15,16);
        // Add at second position
        getFixture().addAll(1, listToAdd);
        SoftAssertions.assertSoftly(
                softly-> {
                    assertThat(getFixture())
                        .as("Size of list is 4 after addAll operation")
                        .hasSize(4);
                    assertThat(getFixture().get(1))
                        .as("15 is present as index 1")
                        .isEqualTo(15);
                    assertThat(getFixture().get(2))
                        .as("16 is present as index 2")    
                        .isEqualTo(16);
                    assertThat(getFixture().get(3))
                        .as("11 is present as index 3")    
                        .isEqualTo(11);
                    
                });
        assertThatExceptionOfType(IndexOutOfBoundsException.class)
            .as("OOBE is thrown when index > size of list")    
            .isThrownBy(()-> {
            getFixture().addAll(5, listToAdd);
        });
    }

    @Test 
    public void testEquals() {
        // Using local fixture for ease of comparison.
        List<Integer> localFixture = Arrays.asList(11, 12);
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(localFixture.equals(Arrays.asList(11, 12)))
                        .as("List of same kind with same elements should return as equal")
                        .isTrue();
                softly.assertThat(localFixture.equals(Arrays.asList(11)))
                        .as("A list is not equal to if it has subset of elements")
                        .isFalse();
                softly.assertThat(localFixture.equals(Arrays.asList(11, 12, 13)))
                        .as("A list is not equal to another list that contains extra elements")
                        .isFalse();
                softly.assertThat(localFixture.equals(Arrays.asList(12, 11)))
                        .as("A list is not equal to another list where the order of elements is different")
                        .isFalse();
            });
    }

    @Test 
    public void testHashcode() {
        // Using local fixture for ease of comparison.
        List<Integer> localFixture = Arrays.asList(11, 12);
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(localFixture.hashCode())
                        .as("List of same kind with same elements should return same hashcode")
                        .isEqualTo(Arrays.asList(11, 12).hashCode());
                softly.assertThat(localFixture.hashCode())
                        .as("A list that has sub set of elements does not have same hashcode")
                        .isNotEqualTo(Arrays.asList(11).hashCode());
                softly.assertThat(localFixture.hashCode())
                        .as("A list does not have same hashcode as another list that contains extra elements")
                        .isNotEqualTo(Arrays.asList(11, 12, 13).hashCode());
                softly.assertThat(localFixture.hashCode())
                        .as("A list does not have same hashcode as another list where the order of elements is different")
                        .isNotEqualTo(Arrays.asList(12, 11).hashCode());
            });
    }

    @Test 
    public void testIndexOf() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().indexOf(11))
                        .as("Index of 11 is 1")
                        .isEqualTo(1);
                softly.assertThat(getFixture().indexOf(15))
                        .as("index of non-existent element is -1")
                        .isEqualTo(-1);
                getFixture().add(11);
                softly.assertThat(getFixture().indexOf(11))
                    .as("index of first occurence is returned by indexOf")
                    .isEqualTo(1);
            });
    }

    @Test 
    public void testLastIndexOf() {
        SoftAssertions.assertSoftly(
            softly-> {
                getFixture().add(11);
                softly.assertThat(getFixture().lastIndexOf(11))
                        .as("lastIndexOf returns the last occurence of the element")
                        .isEqualTo(2);
                softly.assertThat(getFixture().lastIndexOf(15))
                        .as("index of non-existent element is -1")
                        .isEqualTo(-1);
            });
    }

    @Test 
    public void testRemoveElementAt() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().contains(11))
                    .as("The fixture contains 11")
                    .isTrue();
                // This can be a little confusing as 2 can be autoboxed.
                // As there is a matching method for a primitive, it will be used.
                getFixture().remove(1);
                softly.assertThat(getFixture())
                    .as("The size of fixture is reduced by 1")
                    .hasSize(1);
                softly.assertThat(getFixture())
                    .as("fixture no longer contains 11")
                    .doesNotContain(11);
            });
    }

    @Test 
    public void testSet() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().get(1))
                    .as("The fixture contains 11 at position 1")
                    .isEqualTo(11);
                getFixture().set(1, 12);
                softly.assertThat(getFixture())
                    .as("The size of fixture has not changed")
                    .hasSize(2);
                softly.assertThat(getFixture().get(1))
                    .as("fixture has 12 set at position 1")
                    .isEqualTo(12);
            });
    }

    @Test 
    public void testReplaceAll() {
        SoftAssertions.assertSoftly(
            softly-> {
                // replace all the elements by incrementing them by 1
                getFixture().replaceAll(item-> ++item);
                softly.assertThat(getFixture())
                    .as("The size of fixture has not changed")
                    .hasSize(2);
                softly.assertThat(getFixture())
                    .as("The values have been incremented by 1")
                    .isEqualTo(Arrays.asList(11, 12));
            });
    }

    @Test 
    public void testSort() {
        SoftAssertions.assertSoftly(
            softly-> {
                // The fixture is sorted in natural order
                // Sort it in descending order
                getFixture().sort(Comparator.reverseOrder());
                softly.assertThat(getFixture())
                    .as("The size of fixture has not changed")
                    .hasSize(2);
                softly.assertThat(getFixture())
                    .as("The values have been ordered in descending order")
                    .isEqualTo(Arrays.asList(11, 10));
            });
    }

    
    public void testSubList() {
        // FOr this test we need a list larger then our regular fixture.
        getFixture().add(12);
        List<Integer> subList = getFixture().subList(1, 3);
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(subList)
                    .as("The size of fixture has not changed")
                    .hasSize(2);
                softly.assertThat(getFixture())
                    .as("The has the correct values")
                    .isEqualTo(Arrays.asList(11, 12));
                // Non-structural changes in the list should be reflected in sublist
                getFixture().set(1, 14);
                softly.assertThat(subList.get(1))
                    .as("First element in subList is 14")
                    .isEqualTo(14);
                // Non-structural changes in the sub list should be reflected in list
                subList.set(0, 11);
                softly.assertThat(getFixture().get(1))
                    .as("Second element in subList is 11")
                    .isEqualTo(11);
            });
    }
    
    /*
     * Methods untested.
     * 
        ListIterator<E> listIterator​()
        ListIterator<E> listIterator​(int index)
     */
}
