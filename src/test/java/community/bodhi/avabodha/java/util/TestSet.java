package community.bodhi.avabodha.java.util;

import java.util.Set;

/**
 * Abstract class that encapsulates common test cases across all Sets
 * 
 * @author shrivallabh@rjds.in
 *
 * @param <T> The type of Set being used as SUT
 */
public abstract class TestSet<T extends Set<Integer>> extends  TestCollection<T> {
	

}
