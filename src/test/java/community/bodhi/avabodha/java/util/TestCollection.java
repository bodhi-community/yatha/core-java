package community.bodhi.avabodha.java.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

/**
 * Abstract class to provide tests cases for the methods implemented
 * by every collection. The actual collection is instantiated in the concrete
 * test classes.
 *
 * @author shrivallabh@rjds.in
 * 
 * @param <T> The type of the collection being tested.
 */
public abstract class TestCollection<T extends Collection<Integer>> {

	/**
	 * The reference to hold the SUT.
	 */
    private T fixture;

    /**
     * Implemented by concrete test classes. Used to create the SUT
     * @return
     */
    abstract protected T createFixtureInstance();

    /**
     * The name of class of SUT. Used for diagnostic messages.
     * @return
     */
    abstract protected String collectionClassName();

    /**
     * This should not get overridden so as not to break the
     * tests in this class.
     */
    @Before
    public void setup() {
        // Creates default fixture that works with generic tests.
        // using this fixtures in tests is optional and therefore
        // should not result in fragile tests smell.
        
        // However, changing this fixture should be done with care as 
        // it can be disruptive change.
        fixture=createFixtureInstance();
        fixture.add(10);
        fixture.add(11);
    }
    
    /**
     * Should be used to access the SUT everywhere. Direct access to
     * instance should be avoided.
     * @return
     */
    protected T getFixture() {
        return fixture;
    }


    @Test
    public void testAdd() {
    	getFixture().add(12);
        SoftAssertions.assertSoftly(
                softly-> {
                    softly.assertThat(getFixture())
                        .as("Size of %s is now 3", collectionClassName())
                        .hasSize(3);
                    softly.assertThat(getFixture())
                        .as("12 is present in the %s", collectionClassName())
                        .contains(12);
                });
    }

    @Test
    public void testAddAll() {
        List<Integer> listToAdd=Arrays.asList(15,16);
        getFixture().addAll(listToAdd);
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture())
                      .as("The size of fixture is 4 after addAll")
                      .hasSize(4);
                softly.assertThat(getFixture())
                      .as("The fixture contains elements from the parameter after "
                              + "addAll operation")
                      .contains(15,16);
            });            
    }

    @Test
    public void testClear() {
        assertThat(getFixture()).isNotEmpty();
        getFixture().clear();
        assertThat(getFixture()).isEmpty();
    }

    @Test
    public void testContains() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().contains(11))
                    .as("11 is present in fixture")
                    .isTrue();
                softly.assertThat(getFixture().contains(15))
                    .as("15 is absent in fixture")
                    .isFalse();
            });
    }

    @Test
    public void testContainsAll() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().containsAll(Arrays.asList(10, 11)))
                    .as("11 & 12 are present in fixture")
                    .isTrue();
                softly.assertThat(getFixture().containsAll(Arrays.asList(11, 14)))
                    .as("Check for presence of both 11 *and* 14 returns false")
                    .isFalse();
                softly.assertThat(getFixture().containsAll(Arrays.asList(15, 14)))
                    .as("Check for presence of both 15 and 14 returns false")
                    .isFalse();
            });
    }

    @Test
    public void testIsEmpty() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().isEmpty())
                        .as("isEmpty returns false for non-empty %s", collectionClassName())
                        .isFalse();
                softly.assertThat(Collections.EMPTY_LIST.isEmpty())
                        .as("isEmpty returns true for empty %s", collectionClassName())
                        .isTrue();                
            });
    }

    @Test
    public void testRemoveElement() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().contains(11))
                    .as("The fixture contains 11")
                    .isTrue();
                getFixture().remove(Integer.valueOf(11));
                softly.assertThat(getFixture())
                    .as("The size of fixture is reduced by 1")
                    .hasSize(1);
                softly.assertThat(getFixture())
                    .as("fixture no longer contains 11")
                    .doesNotContain(11);
            });
    }

    @Test
    public void testRemoveAll() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().contains(11))
                    .as("The fixture contains 11")
                    .isTrue();
                getFixture().removeAll(Arrays.asList(11, 12));
                softly.assertThat(getFixture())
                    .as("The size of fixture is reduced by 1")
                    .hasSize(1);
                softly.assertThat(getFixture())
                    .as("fixture no longer contains 11")
                    .doesNotContain(11);
            });
    }

    @Test
    public void testRetainAll() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture().contains(10))
                    .as("The fixture contains 10")
                    .isTrue();
                getFixture().retainAll(Arrays.asList(11, 12));
                softly.assertThat(getFixture())
                    .as("The size of fixture is reduced by 1")
                    .hasSize(1);
                softly.assertThat(getFixture())
                    .as("fixture no longer contains 10 that was not in the collection "
                            + "passed as parameter")
                    .doesNotContain(10);
                softly.assertThat(getFixture())
                    .as("fixture retained 11 that was in the collection passed as parameter.");
            });
    }

    /*
     * Methods untested.
     * 
        Iterator<E> iterator​()
        default Spliterator<E> spliterator​()
        Object[] toArray​()
        <T> T[] toArray​(T[] a)
     */
}