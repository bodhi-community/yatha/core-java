package community.bodhi.avabodha.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * 
 * @author shrivallabh@rjds.in
 *
 */
@RunWith(Parameterized.class)
public class TestIterator {
	private Collection<Integer> fixtureCollection;
	
	
	
	public TestIterator(Collection<Integer> fixture) {
		this.fixtureCollection = fixture;
	}

	@Parameters
    public static Collection<Collection<Integer>> data() {
        return Arrays.asList(new ArrayList<>(), 
        					 new LinkedList<>(), 
        					 new HashSet<>());
    }

	protected Collection<Integer> getFixtureCollection() {
		return fixtureCollection;
	}

	@Before
	public void setup() {
		// The collection gets set during construction. The object will persist 
		// as though it has been created in @BeforeClass
		fixtureCollection.add(5);
		fixtureCollection.add(1);
		fixtureCollection.add(7);
		fixtureCollection.add(9);
		fixtureCollection.add(4);
	}
	
	@Test
	public void testHasNext() {
		SoftAssertions.assertSoftly(
				softly-> {
					Iterator<Integer> sut = fixtureCollection.iterator();
					softly.assertThat(sut)
						.as("Iterator should have an element at beginning of iteration of non-empty "
								+ "Collection")
						.hasNext();
					fixtureCollection.clear();
					// To avoid concurrent changes during iteration, we need to reinitialize
					// sut.
					sut = fixtureCollection.iterator();
					softly.assertThat(sut.hasNext())
						.as("Iterator should not have an element at beginning of iteration of non-empty "
								+ "Collection")
						.isFalse();			
				});
	}

	
	
	@Test
	public void testRemove() {
		Iterator<Integer> sut = fixtureCollection.iterator();
		SoftAssertions.assertSoftly(
				softly-> {
					// move iterator forward
					sut.next();
					sut.remove();
					softly.assertThat(fixtureCollection)
						.as("The size of collection should reduce to 4")
						.hasSize(4);	
				});
	}

	// Write test case for for each remaining.
}
