package community.bodhi.avabodha.java.util;

import java.util.HashSet;


/**
 * 
 * @author shrivallabh@rjds.in
 *
 */
public class TestHashSet extends TestSet<HashSet<Integer>> {

	@Override
	protected HashSet<Integer> createFixtureInstance() {
		return new HashSet<Integer>();
	}

	@Override
	protected String collectionClassName() {
		return "HashSet";
	}

}
