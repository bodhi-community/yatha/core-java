package community.bodhi.avabodha.java.util;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * 
 * @author shrivallabh@rjds.in
 *
 */
@RunWith(Parameterized.class)
public class TestListIterator {
	
	private List<Integer> fixtureCollection;

	public TestListIterator(List<Integer> fixture) {
		this.fixtureCollection = fixture;
	}

	@Parameters
    public static Collection<List<Integer>> dataListIterator() {
        return Arrays.asList(new ArrayList<>(), new LinkedList<>());
    }
	
	
	@Before
	public void setup() {
		// The collection gets set during construction. The object will persist 
		// as though it has been created in @BeforeClass
		fixtureCollection.clear();
		fixtureCollection.add(5);
		fixtureCollection.add(1);
		fixtureCollection.add(7);
		fixtureCollection.add(9);
		fixtureCollection.add(4);
	}
	
	@Test
	public void testHasNext() {
		SoftAssertions.assertSoftly(
				softly-> {
					ListIterator<Integer> sut = fixtureCollection.listIterator();
					softly.assertThat(sut)
						.as("Iterator should have an element at beginning of iteration of non-empty "
								+ "Collection")
						.hasNext();
					// To avoid concurrent changes during iteration, we need to reinitialize
					// sut.
					sut = fixtureCollection.listIterator();
					fixtureCollection.clear();
					softly.assertThat(sut.hasNext())
						.as("Iterator should not have an element at beginning of iteration of non-empty "
								+ "Collection")
						.isFalse();			
				});
	}
	
	/**
	 * 
	 */
	@Test
	public void testNextIndex() {
		SoftAssertions.assertSoftly(
				softly-> {
					ListIterator<Integer> sut = fixtureCollection.listIterator();
					softly.assertThat(sut.nextIndex())
						.as("Iterator should return item at index 0 at beginning of iteration")
						.isEqualTo(0);
					while(sut.hasNext()) 
						sut.next();
					softly.assertThat(sut.nextIndex())
						.as("After iterator is exhausted nextIndex should return "
								+ "index equal to size of collection")
						.isEqualTo(5);			
				});
	}


	@Test
	public void testAdd() {
		ListIterator<Integer> sut = fixtureCollection.listIterator();
		SoftAssertions.assertSoftly(
				softly-> {
					// Move to second position
					sut.next();
					sut.add(12);
					softly.assertThat(fixtureCollection)
						.as("The size of collection should increase to 6")
						.hasSize(6);
					softly.assertThat(fixtureCollection.get(1))
						.as("Check the element at 2nd position is 12")
						.isEqualTo(12);
				});
	}
	
	@Test
	public void testRemove() {
		ListIterator<Integer> sut = fixtureCollection.listIterator();
		SoftAssertions.assertSoftly(
				softly-> {
					// Move index forward
					sut.next();
					sut.remove();
					softly.assertThat(fixtureCollection)
						.as("The size of collection should reduce to 4")
						.hasSize(4);
					softly.assertThat(sut.next())
						.as("next() should return second element the value of which is 1")
						.isEqualTo(1);
				});
	}
	
	@Test
	public void testHasPrevious() {
		ListIterator<Integer> sut = fixtureCollection.listIterator();
		SoftAssertions.assertSoftly(
				softly-> {
					softly.assertThat(sut.hasPrevious())
						.as("Iterator should not have a previous element at beginning of "
								+ "iteration of Collection")
						.isFalse();
					sut.next();
					softly.assertThat(sut.hasPrevious())
						.as("Iterator should have an element when the iterator is at"
								+ " index 1")
						.isTrue();			
				});
	}

	@Test
	public void testPreviousIndex() {
		SoftAssertions.assertSoftly(
				softly-> {
					ListIterator<Integer> sut = fixtureCollection.listIterator();
					softly.assertThat(sut.previousIndex())
						.as("previousIndex should return item -1 at beginning of iteration")
						.isEqualTo(-1);
					sut.next();
					softly.assertThat(sut.previousIndex())
						.as("After moving to index 1, previousIndex should return 0")
						.isEqualTo(0);			
				});
	}

	@Test
	public void testPrevious() {
		ListIterator<Integer> sut = fixtureCollection.listIterator();
		assertThatExceptionOfType(NoSuchElementException.class)
			.as("Previous should throw an exception if called at beginning of iteration")
			.isThrownBy(() -> sut.previous());
		SoftAssertions.assertSoftly(
				softly-> {
					Integer item = sut.next();
					softly.assertThat(sut.previous())
						.as("previous should return the last item returned by"
								+ " the call to next method of the iterator")
						.isEqualTo(item);
				});
	}
	
	/*
	 * E previous​()
	 * Returns the previous element in the list and moves the cursor position
	 * backwards. 
	 * int previousIndex​() Returns the index of the element that would
	 * be returned by a subsequent call to previous(). 
	 * void remove​() Removes from
	 * the list the last element that was returned by next() or previous() (optional
	 * operation). void set​(E e) Re
	 */
}
