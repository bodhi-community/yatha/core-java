package community.bodhi.avabodha.java.util;

import java.util.PriorityQueue;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

/**
 * 
 * @author shrivallabh@rjds.in
 *
 */
public class TestPriorityQueue extends TestCollection<PriorityQueue<Integer>> {

    @Override
    protected PriorityQueue<Integer> createFixtureInstance() {
        return new PriorityQueue<Integer>();
    }

    @Override
    protected String collectionClassName() {
        return "Priority queue";
    }

    @Test
    public void testPeek() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture())
                    .as("Priority queue has the size of 2")
                    .hasSize(2);
                softly.assertThat(getFixture().peek())
                    .as("The element peeked has lowest integer value of 10")
                    .isEqualTo(10);
                softly.assertThat(getFixture())
                    .as("Priority queue has the size of 2 after peek operation")
                    .hasSize(2);
            });
    }
    
    
    @Test
    public void testOffer() {
        SoftAssertions.assertSoftly(
                softly-> {
                    softly.assertThat(getFixture())
                        .as("Priority queue has the size of 2")
                        .hasSize(2);
                    getFixture().offer(9);
                    softly.assertThat(getFixture())
                        .as("Priority queue has the size of 3 after offer is called")
                        .hasSize(3);
                    softly.assertThat(getFixture().peek())
                        .as("The element offered has lowest integer value of 9")
                        .isEqualTo(9);
                });
        
    }
    
    @Test
    public void testPoll() {
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(getFixture())
                    .as("Priority queue has the size of 2")
                    .hasSize(2);
                softly.assertThat(getFixture().poll())
                    .as("The element polled has lowest integer value of 10")
                    .isEqualTo(10);
                softly.assertThat(getFixture())
                    .as("Priority queue has the size of 1 after peek operation")
                    .hasSize(1);
            });
    }
    /**
     * TODO:
     * comparator methods not tested
     */
}
