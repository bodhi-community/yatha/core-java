package community.bodhi.avabodha.java.lang;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertThrows;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Comparator;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;


/* 
 * @author omkar.harkare@rjds.in
 * 
 */

public class TestMethods {
	
	
	@Test
	public void testGetAnnotatedReturnType()  {
			try {
				AnnotatedType anot = Class.forName(SampleClass.getFullName())
						.getMethod("getSampleField", null).getAnnotatedReturnType();
				assertThat(anot.getType().getTypeName())
					.as("The method type is String ")
					.isEqualTo("java.lang.String");
			} catch (NoSuchMethodException e) {
				fail("The method loading should have succeeded, Unexpected error", e);
			} catch (SecurityException e) {
				fail("A security exception occured while getting method", e);		
			} catch (ClassNotFoundException e) {
				 fail("A Class loading should have succeeded, Unexpected error", e);
				
			}		
		
	}
	
	@Test
	public void testGetAnnotation() {
		try {
			Method methods = TestMethods.class.getMethod("testGetAnnotatedReturnType", null);
			assertThat(methods.getAnnotation(Override.class))
				.as("Override annotation for getAnnotatedReturnType" + "should be null ")
				.isNull();
			assertThat(methods.getAnnotation(Test.class).annotationType().getName())
				.as("Test annotation for getAnnotatedReturnType")
				.isNotNull()
				.isEqualTo("org.junit.Test");
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}		
		
	}
	
	@Test
	public void testGetDeclaredAnnotations() {
		try {
			Method methods = TestMethods.class.getMethod("testHashCode", null);
			SoftAssertions.assertSoftly(
					softly -> {
						Annotation[] sut = methods.getDeclaredAnnotations();
						softly.assertThat(sut.length)
							.as("Only one annotation is initialised ")
							.isEqualTo(1);
						softly.assertThat(sut[0].annotationType().isAnnotation())
							.isTrue();
						softly.assertThat(sut[0].annotationType().toString())
							.isEqualTo("interface org.junit.Test");
						softly.assertThat(sut[0].annotationType().getName())
							.as("The annotation is org.junit.Test ")
							.isEqualTo("org.junit.Test");
			});
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
	
		}		
		
			
	}
	
	@Test
	public void testGetDeclaringClass()  {
		try {
			Method methods = Integer.class.getMethod("compareTo", Integer.class);
			assertThat(methods.isVarArgs())
				.as("Can take variable of arguments ")
				.isFalse();
			assertThat(methods.getDeclaringClass().getName())
				.as("The declaring class is Integer")
				.isEqualTo("java.lang.Integer");
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testGetDefaultValue() {
		try {
			Method methods = Float.class.getMethod("equals", Object.class);
			assertThat(methods.getDefaultValue())
				.as("Default value is null unless specified ")
				.isNull();
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}

	@Test
	public void testGetExceptionTypes()  {
		try {
			Method methods = Class.class.getMethod("forName", String.class);
			Class<?>[] sut = methods.getExceptionTypes();
			assertThat(sut.length)
				.as("Throws only one exception ")
				.isEqualTo(1);
			assertThat(sut[0].getTypeName().toString())
				.as("Throws java.lang.ClassNotFoundException ")
				.isEqualTo("java.lang.ClassNotFoundException");
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));
		
	}
	
	@Test
	public void testGetGenericExceptionTypes() {
		try {
			Method methods = Class.class.getMethod("getDeclaringClass", null);
			Type[] sut = methods.getGenericExceptionTypes();
			assertThat(sut.length)
				.as("Throws only one Exception")
				.isEqualTo(1);
			assertThat(sut[0].getTypeName())
				.as("Throws SecurityException")
				.isEqualTo("java.lang.SecurityException");
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));	
		
	}
	
	@Test
	public void testGetGenericParameterTypes()  {
		try {
			Method methods = Long.class.getMethod("equals", Object.class);
			Type[] sut = methods.getGenericParameterTypes();
			assertThat(sut.length)
				.as("Takes only one parameter")
				.isEqualTo(1);
			assertThat(sut[0].getTypeName().toString())
				.isEqualTo("java.lang.Object");
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	
	}
	
	@Test
	public void testGetGenericReturnType() {
		try {
			Method methods = Float.class.getMethod("longValue", null);
			Type sut = methods.getGenericReturnType();
			assertThat(sut.toString())
				.as("Returns Long value")
				.isEqualTo("long");
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testGetModifiers() {
		try {
			assertThat(Float.class.getMethod("intValue", null).getModifiers())
				.as("Only one modifier and type")
				.isEqualTo(1);
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
			
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testGetName()  {
		try {
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestMethods").getMethods())
				.extracting(method -> method.getName())
				.asList()
				.as("Getting the already Declared methods")
				.contains("testGetParameterCount",
						  "testGetModifiers",
						  "testGetGenericReturnType",
						  "testGetGenericParameterTypes",
						  "testGetAnnotatedReturnType");
		} catch (ClassNotFoundException e) {
			fail("The Class loading should have succeeded, Unexpected error", e);
			
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testGetParameterCount() throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestClass")
					.getDeclaredMethod("testCast").getParameterCount())
			.as("Takes no paramaters")
			.isEqualTo(0);
		assertThat(Integer.class.getMethod("decode", String.class)
				.getParameterCount())
			.as("Takes one parameter")
			.isEqualTo(1);
	
	}
	
	@Test
	public void testGetParameterTypes()  {
		try {
			Class<?>[] sut = Integer.class.getMethod("bitCount", int.class).getParameterTypes();
			assertThat(sut.length)
				.as("Accepts one parameter only")
				.isEqualTo(1);
			assertThat(sut[0].toString())
				.isEqualTo("int");
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);	
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}		
		
	}
	
	@Test
	public void testGetReturnType()  {
		try {
			assertThat(Integer.class.getMethod("compareTo", Integer.class)
					.getReturnType().toString())
				.as("The method returns static int ")
				.isEqualTo("int");
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testGetTypeParameters() {
		try {
			TypeVariable<Method>[] method = Integer.class
					.getMethod("equals", Object.class).getTypeParameters();
			assertThat(method.length).
				as("Underlying generic declaration declares no typevariables")
				.isEqualTo(0);
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
		
	}
	
	@Test
	public void testHashCode() {
		try {
			Method method_1 = Integer.class.getMethod("floatValue", null);
			Method method_2 = Long.class.getMethod("floatValue", null);
			assertThat(method_1.hashCode())
				.as("Different hashcode value for different class")
				.isNotEqualTo(method_2.hashCode());
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
			
		}		
	}
	
	@Test
	public void testIsDefault() {
		try {
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestMethods")
					.getMethod("testHashCode", null).isDefault())
				.isFalse();
			// Asserting a default method
			assertThat(Comparator.class.
					getMethod("thenComparing", Comparator.class).isDefault())
				.isTrue();
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loadind should have succeeded, Unexpected error", e);
			
		}	
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));
		
	}
	
	@Test
	public void testIsSynthetic() {
		try {
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestMethods")
					.getMethod("testGetReturnType", null).isSynthetic())
				.as("Loading the test class itself")
				.isFalse();
			//Asserting a synthetic method
			assertThat(Integer.class.
					getMethod("compareTo", Object.class).isSynthetic())
				.as("compareTo is synthetic method")
				.isTrue();
			
		} catch (NoSuchMethodException e) {
			 fail("The Class loading should have succeeded, Unexpected error", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loadind should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		}	
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));
	}
	
	@Test
	public void testVarArgs() {
		try {
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestMethods")
					.getMethod("testVarArgs", null).isVarArgs())
				.isFalse();
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error");	
		} catch (ClassNotFoundException e) {
			fail("A Class loadind should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}	
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));

	}
	
	@Test
	public void testToGenericString() {
		try {
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestMethods")
					.getMethod("testHashCode", null).toGenericString())
				.as("Loading the test class itself")
				.isEqualTo("public void community.bodhi.avabodha.java.lang.TestMethods.testHashCode()");
			assertThat(Class.class.getMethod("getAnnotatedInterfaces", null)
					.toGenericString())
				.isEqualTo("public java.lang.reflect.AnnotatedType[] java.lang.Class.getAnnotatedInterfaces()");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loadind should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		}	
		assertThrows("Trying to load non existent Class",
				ClassNotFoundException.class,  
                ()-> Class.forName("does.not.Exist"));

	}
	
	@Test
	public void testGetParameterAnnotations() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getDeclaredMethod("setSampleField", String.class)
					.getParameterAnnotations().length)
				.isEqualTo(1);
			assertThat(Class.forName(SampleClass.getFullName())
					.getDeclaredMethod("setSampleField", String.class)
					.getParameterAnnotations()[0][0])
					.asString()
					.as("PathVar is defined as parameter annotation ")
					.isEqualTo("@community.bodhi.avabodha.java.lang.PathVar()");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			fail("A Class loading should have succeeded, Unexpected error", e);
			
		}
		
	}
	
	@Test
	public void testIsBridge() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getMethod("getClass", null).isBridge())
				.isFalse();
			assertThat(Class.forName(SampleClass.getFullName())
					.getMethod("setSampleField", String.class).isBridge())
				.isFalse();	
			
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			fail("A Class loading should have succeeded, Unexpected error", e);
			
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testSetAccessible() {
		try {
			Method sut = Class.forName(SampleClass.getFullName())
					.getMethod("setSampleField", String.class);
			assertThat(sut.isAccessible())
				.as("Initially method is not accessible")
				.isFalse();
			sut.setAccessible(true);
			assertThat(sut.isAccessible())
				.isTrue();
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			fail("A Class loading should have succeeded, Unexpected error", e);
			
		}
	}
	
}
 			
 