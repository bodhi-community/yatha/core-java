package community.bodhi.avabodha.java.lang;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertThrows;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Target;
import java.lang.reflect.AnnotatedType;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

public class TestClass {

    @Test 
    public void testCast() {
        Collection<Integer> sut = new ArrayList<>();
        try {
            List<Integer> correctCast = List.class.cast(sut);            
        } catch(Exception e) {
            fail("The cast should have succeeded. Unexpected exception", e);
        }
        assertThrows("List cannot be cast as Set", 
                     ClassCastException.class,  
                     ()-> Set.class.cast(sut));
    }
    
    @Test 
    public void testForName​() {
        try {
            assertThat(Class.forName("community.bodhi.avabodha.java.lang.TestClass"))
                .as("Load the test class itself.")
                .isEqualTo(this.getClass());
        } catch (ClassNotFoundException e) {
            fail("The Class loading should have succeeded. Unexpected exception", e);
        }
        assertThrows("Trying to load non-existent class", 
                     ClassNotFoundException.class,  
                     ()-> Class.forName("does.not.Exist"));
    }
    
    @Test 
    public void testGetAnnotatedInterfaces() {
        assertThat(Arrays.stream(ArrayList.class.getAnnotatedInterfaces())
                        .map(atype->atype.getType().getTypeName())
                        .collect(Collectors.toSet()))
            .as("The set of annotated interfaces for ArrayList")
            .containsOnly("java.util.List<E>", 
                          "java.util.RandomAccess",
                          "java.lang.Cloneable",
                          "java.io.Serializable");
        assertThat(Iterable.class.getAnnotatedInterfaces())
            .as("Iterable interface has no annotated super interfaces.")
            .isEmpty();
    }

    @Test 
    public void testGetAnnotatedClass() {
        assertThat(ArrayList.class.getAnnotatedSuperclass().getType().getTypeName())
            .isEqualTo("java.util.AbstractList<E>");
        assertThat(Object.class.getAnnotatedSuperclass())
            .isNull();
    }
    
    @Test
    public void TestGetAnnotation() {
        assertThat(Runnable.class.getAnnotation(FunctionalInterface.class))
            .as("Retrieve Functional interface annotation for Runnable")
            .isNotNull();
        
        assertThat(Runnable.class.getAnnotation(Target.class))
            .as("Target annotation for Runnable "
                    + "interface should be null")
            .isNull();
    }

    @Test
    public void TestGetAnnotations() {
        Annotation[] annotations = Runnable.class.getAnnotations();
        SoftAssertions.assertSoftly(
            softly-> {
                softly.assertThat(annotations)
	                .as("Retrieve all annotation for Runnable")
	                .hasSize(1);
                softly.assertThat(annotations[0].toString())
        			.as("The entry is FunctionalInterface annotation")
        			.isEqualTo("@java.lang.FunctionalInterface()");
                softly.assertThat(Object.class.getAnnotations())
                	.as("Object class does not have any annotations")
                	.isEmpty();
            });
    }

    @Test
    public void TestGetCanonicalName() {
    	assertThat(List.class.getCanonicalName())
    		.as("Match canonical class name of list with the class")
    		.isEqualTo("java.util.List");
    }
}
