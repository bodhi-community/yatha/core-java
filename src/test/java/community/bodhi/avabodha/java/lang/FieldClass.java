package community.bodhi.avabodha.java.lang;
import java.lang.annotation.*;

public abstract class FieldClass {
	 
		public static float war = 39.0f;  
        public static boolean isZero = false; 
        public static boolean isOne = true;
        public static double value = 24;
        public static int work = 13134; 
        public static char core = 'A';
        public static long set = 43;
        public static short unique = 239;
        public static byte code = 12;
        @annotations("ChipherCodes@#!") 
        private double string; 
        
        @Target({ ElementType.FIELD }) 
        @Retention(RetentionPolicy.RUNTIME) 
        private @interface annotations { 
            String value(); 
        }     
        
} 

	