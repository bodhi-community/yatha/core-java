package community.bodhi.avabodha.java.lang;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertThrows;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.mockito.Mock;


/*
 * @author omkar.harkare@rjds.in
 * 
 */
public class TestConstructor {
	
	
	@Test
	public void testEquals() {
		assertThat(SampleClass.class.getConstructors().length)
			.isEqualTo(2);
		try {
			assertThat(Class.forName(SampleClass.getFullName()).getConstructor(null)
					.equals(SampleClass.class.getConstructor(String.class)))
				.as("Both Constructors take different parameters")
				.isFalse();
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
		}
		
	}
		
	@Test
	public void testGetAnnotatedReturnType() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(null).getAnnotatedReturnType()
					.getType()).asString()
				.isEqualTo("class community.bodhi.avabodha.java.lang.SampleClass");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		} 
	}
	
	@Test
	public void testGetAnnotation() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors().length)
				.isEqualTo(2);
			assertThat(SampleClass.class.getConstructor(null)
					.getAnnotation(Custom.class).toString())
				.isEqualTo("@community.bodhi.avabodha.java.lang.Custom(value=\"Constructor\")");
		}catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}catch (ClassNotFoundException e) {
			fail("A Class loading should have succeeded, Unexpected error", e);
		} catch (NoSuchMethodException e) {
			fail("The method loading should have succeeded, Unexpected error", e);
			
			
		} 
		
	}
	
	@Test
	public void testGetDeclaredAnnotation() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(null).getDeclaredAnnotation(Custom.class).toString())
				.as("Custom annotation is initialised")
				.isEqualTo("@community.bodhi.avabodha.java.lang.Custom(value=\"Constructor\")");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		}  
		
	}
	
	@Test
	public void testGetDeclaringClass() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors()[1].getDeclaringClass()).asString()
				.isEqualTo("class community.bodhi.avabodha.java.lang.SampleClass");
	
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
		
		} 
	}
	
	@Test
	public void testGetExceptionTypes() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors().length)
				.isEqualTo(2);			
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.SampleClass")
					.getConstructors()[1].getExceptionTypes().length)
				.as("No exception declared to be thrown ")
				.isEqualTo(0);
		}catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}catch (ClassNotFoundException e) {
			fail("A Class loading should have succeeded, Unexpected error", e);
		}
		
	}
	
	@Test
	public void testGetGenericExceptionTypes() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).getGenericExceptionTypes())
				.as(" the underlying executable declaresno exceptions ")
				.isEmpty();
			
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		 }catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		}  
		
	}
	
	@Test
	public void testGetGenericParameterTypes() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).getGenericParameterTypes())
				.isNotEmpty();
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.SampleClass")
					.getConstructor(String.class).getGenericParameterTypes()[0])
				.as("Takes only one parameter")
				.asString()
				.isEqualTo("class java.lang.String");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		}  
	}
	
	@Test
	public void testGetModifiers() {
		try {
			assertThat(SampleClass.class.getConstructor(String.class).getModifiers())
				.isEqualTo(1);
			assertThat(Integer.class.getConstructor(int.class).getModifiers())
				.isEqualTo(1);
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		}
	}
	
	@Test
	public void testGetName() {
		assertThat(SampleClass.class.getConstructors().length)
			.isEqualTo(2);

		assertThat(SampleClass.class.getConstructors())
			.extracting(name -> name.getName())
			.asList()
			.as("Loading sut class")
			.contains("community.bodhi.avabodha.java.lang.SampleClass");
	}
	
	@Test
	public void testGetParameterAnnotations() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).getParameterAnnotations().length)
				.isEqualTo(1);
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).getParameterAnnotations()[0].length)
				.isEqualTo(1);
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).getParameterAnnotations()[0][0])
				.asString()
				.as("PathVar is defined as parameter annotation ")
				.isEqualTo("@community.bodhi.avabodha.java.lang.PathVar()");
		
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		}  
	}
	
	@Test
	public void testGetParameterCount() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(null)
					.getParameterCount())
				.isEqualTo(0);
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class)
					.getParameterCount())
				.as("Takes no parameters ")
				.isEqualTo(1);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);	
		} catch (NoSuchMethodException e) {
			fail("The Method loading should have succeeded, Unexpected error", e);
		}  
	}
	
	@Test
	public void testGetTypeParameters() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors()[0]
					.getTypeParameters())
				.as("The underlying generic declaration declares no typevariables ")
				.isEmpty();
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors()[1]
					.getTypeParameters())
				.as("The underlying generic declaration declares no typevariables ")
				.isEmpty();
			
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
				
		}  
		
	}
	
	@Test 
	public void testIsSynthetic() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors()[1]
					.isSynthetic())
				.isFalse();
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructors()[0]
					.isSynthetic())
				.isFalse();
			
		} catch (SecurityException e) {
			fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
				
		}  
	}
	
	@Test
	public void testIsVarArgs() {
		try {
			assertThat(SampleClass.class.getConstructor(String.class)
					.isVarArgs())
				.as(" Not declared to take variable number of arguments")
				.isFalse();
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		}
	}
	
	@Test
	public void testToGenericString() {
		try {
			assertThat(Class.forName(SampleClass.getFullName())
					.getConstructor(String.class).toGenericString())
				.isEqualTo("public community.bodhi.avabodha.java.lang.SampleClass(java.lang.String)");
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
			
		}  
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testSetAccessible() {
		try {
			Constructor<?> sut = Class.forName(SampleClass.getFullName())
					.getDeclaredConstructor(String.class);
			assertThat(sut.isAccessible())
				.as("Initially constructor is not accessible")
				.isFalse();
			sut.setAccessible(true);
			assertThat(sut.isAccessible())
				.isTrue();
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		} catch (ClassNotFoundException e) {
			 fail("A Class loading should have succeeded, Unexpected error", e);
		}  
	}
	
	@Test(expected = InstantiationException.class)
	public void testNewInstance() throws InstantiationException {
		try {
			Constructor sut = SampleClass.class
					.getDeclaredConstructor();
			SampleClass obj = (SampleClass)sut.newInstance("data");
			assertThat(obj.getSampleField())
				.asString()
				.isEqualTo("data");			
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException error occurred");
		} catch (IllegalArgumentException e) {
			fail("IllegalArgumentException error occurred", e);
		} catch (InvocationTargetException e) {
			fail("InvocationTargetException error occurred", e);
		} catch (NoSuchMethodException e) {
			 fail("The Method loading should have succeeded, Unexpected error", e);
		} catch (SecurityException e) {
			 fail("A security exception occured while getting method", e);
		}  
	}
	

}


