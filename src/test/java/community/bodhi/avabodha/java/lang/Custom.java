package community.bodhi.avabodha.java.lang;

import java.lang.annotation.*;

@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface Custom {
	public String value();
}
