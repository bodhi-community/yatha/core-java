package community.bodhi.avabodha.java.lang;

import static org.assertj.core.api.Assertions.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.rmi.AccessException;

import org.junit.Test;
public class TestParameter {

	@Test
	public void testisImplicit() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters().length)
				.isEqualTo(1);
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters()[0].isImplicit())
			        .isEqualTo(false);
				    }
	
	@Test
	public void testisSynthetic() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters().length)
				    .isEqualTo(1);
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters()[0].isSynthetic())
			        .isEqualTo(false);
				    }
	
	@Test
	public void testisVarArgs() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters()[0].isVarArgs())
			        .isEqualTo(false);
				    }
	
	@Test
	public void testisNamePresent() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
			assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
					.getDeclaredMethod("setSampleField", String.class)
					.getParameters()[0].isNamePresent())
			        .isEqualTo(false);
				    }
	
	@Test 
	public void testgetModifiers() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
				.getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0].getModifiers())
		        .isEqualTo(0);
	}
	
	@Test 
	public void testgetName() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
				.getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0].getName())
		        .isEqualTo("arg0");
	}
	
	@Test 
	public void testgetType() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
				.getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0].getType().getName())
		        .isEqualTo("java.lang.String");
	}
	
	@Test 
	public void testHashcode() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
				.getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0].getType().hashCode())
		        .isEqualTo(349885916);
		        }
	
	@Test 
	public void testgetParamterisedType() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")
				.getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0].getParameterizedType().getTypeName())
		        .isEqualTo("java.lang.String");
	}
	@Test
	public void testGetAnnotatedType() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
				    .getDeclaredMethod("setSampleField", String.class)
					.getParameters()[0]
					.getAnnotatedType().getType()).asString()
				    .isEqualTo("class java.lang.String");
		
		}
	
	@Test
	public void testDeclaringExecutable() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getDeclaringExecutable().getName())
			    .isEqualTo("setSampleField");
			    
	   }
	
	@Test
	public void testgetAnnotation() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getAnnotations())
			    .isEmpty();
}

	@Test
	public void testgetDeclaredAnnotation() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getDeclaredAnnotations())
			    .isEmpty();
}
	
	@Test
	public void testgetDeclaredAnnotationT() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getDeclaredAnnotation(Annotation.class))
			    .isNull();
	}
	
	@Test
	public void testgetAnnotationT() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getAnnotation(Annotation.class))
			    .isNull();
	}
	
	@Test
	public void testgetAnnotationByType() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getAnnotationsByType(Annotation.class))
			    .isEmpty();
	}
	
	@Test
	public void testgetDeclaredAnnotationByType() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		assertThat(Class.forName("community.bodhi.avabodha.java.lang.ParameterClass")					
			    .getDeclaredMethod("setSampleField", String.class)
				.getParameters()[0]
			    .getDeclaredAnnotationsByType(Annotation.class))
			    .isEmpty();
	}
	
	@Test
	public void testEquals() throws NoSuchFieldException, 
	   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException, AccessException,NoSuchMethodException { 
		
		Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.ParameterClass");
	    Method method1 = c.getDeclaredMethod("setSampleModule", int.class);
	    Method method2 = c.getDeclaredMethod("setSampleField", String.class);
	    Parameter parameter1 = method1.getParameters()[0];
	    Parameter parameter2 = method2.getParameters()[0];
	    assertThat(parameter1.equals(parameter2))
	    .isEqualTo(false);
	}			
}
