package community.bodhi.avabodha.java.lang;

import java.lang.annotation.*;

public abstract class SampleClass {

	   private String sampleField;
	   
	   @Custom(value = "Constructor")
	   public SampleClass(){
	   }

	   public SampleClass(@PathVar String sampleField){
	      this.sampleField = sampleField;
	   }

	   public String getSampleField() {
		   System.out.println("System under testing ");
	      return sampleField;
	   }

	   public void setSampleField(@PathVar String sampleField) {
	      this.sampleField = sampleField;
	   }

	public static String getFullName() {
		return "community.bodhi.avabodha.java.lang.SampleClass";
	}
	
}


@Retention(RetentionPolicy.RUNTIME)
@interface PathVar {
	
}


