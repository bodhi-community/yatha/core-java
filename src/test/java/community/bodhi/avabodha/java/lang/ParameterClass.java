package community.bodhi.avabodha.java.lang;

import java.lang.annotation.*;



public abstract class ParameterClass {

	   private String sampleField;
	   private int sampleModule;
	   
	   
	   public ParameterClass(){
	   }
	   public ParameterClass( String sampleField, int sampleModule , double salary){
		      super();
		      this.sampleField = sampleField;
		      this.sampleModule = sampleModule;
		   }
	   public int getSampleModule() {
		   System.out.println("System under testing ");
	      return sampleModule;
	   }

	   public void setSampleModule( int sampleModule) {
	      this.sampleModule = sampleModule;
	   }

	   public String getSampleField() {
		   System.out.println("System under testing ");
	      return sampleField;
	   }

	   public void setSampleField( String sampleField) {
	      this.sampleField = sampleField;
	   }

	}

