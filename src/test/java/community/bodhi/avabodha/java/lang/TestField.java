package community.bodhi.avabodha.java.lang;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Month;
import java.util.Arrays;
import org.junit.Test; 


public class TestField {

	@Test
	 public void testEqual() {
		try {
			Field f = Integer.class.getField("BYTES");
			assertThat(f.equals(Integer.class.getField("SIZE")))
				.as(" both the class have different values ")
				.isFalse();
		} catch (NoSuchFieldException | SecurityException e) {
			fail("Unexpected error", e);
			
		}
	    }
	
	@Test
	public void testObject() throws NoSuchFieldException, 
    SecurityException, IllegalArgumentException, IllegalAccessException {
		Field f = Integer.class.getField("MIN_VALUE");
		assertThat(f.get(f))
		.as("Value of the field MIN_VALUE is returned")
		.isEqualTo(-2147483648);
			
			
		}
	
	@Test
	public void testGetAnnotatedType() throws NoSuchFieldException {
		Field f = Integer.class.getDeclaredField("MIN_VALUE");
		assertThat(f.getAnnotatedType().getType().getTypeName())
		.as("The AnnotatedType returned by the field should be a integer")
	    .isEqualTo("int");
	   }

    @Test
    public void testGetByte() 
		   throws NoSuchFieldException,
           SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException {
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("code");
	   assertThat(field.getByte(field))
	   .as("returns the byte value for the field")
	   .isEqualTo((byte)12);
          
      }
   
    @Test
	public void testGetAnnotated() throws NoSuchFieldException {
		Field f = Integer.class.getDeclaredField("MIN_VALUE");
		assertThat(f.getDeclaredAnnotations())
		.as("The Annotated returned by the field should not be null but empty")
	    .isNotNull()
	    .isEmpty();

     }
   
    @Test
    public void testGetDeclaringClass() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	   Field field = Integer.class.getField("MIN_VALUE");
	   assertThat(field.getDeclaringClass().getName())
	   .as("Reurns the Class object representing class Integer")
	   .isEqualTo("java.lang.Integer");
	}
   
   @Test
   public void testGetDouble() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
	   Field field = Integer.class.getField("SIZE");
	   assertThat(field.getDouble("SIZE"))
	   .as("Returns the double value of the field SIZE")
	   .isEqualTo(32.0);
	  
	 }
   
   @Test
   public void testGetFloat() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
	   Field field = Integer.class.getField("SIZE");
	   assertThat(field.getFloat("SIZE"))
	   .as("Returns the double value of the field SIZE")
	   .isEqualTo(32.0f);
	  
	 }
   @Test 
   public void testGetGenricTyep()  throws NoSuchFieldException {
	   Field field = Integer.class.getField("MIN_VALUE");
	   assertThat(field.getGenericType().getTypeName())
	   .as("Returns the Genric type of the field")
	   .isEqualTo("int");
	  
    }
  
   @Test 
   public void testGetLong() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException { 
	   Field field = Integer.class.getField("SIZE");
	   assertThat(field.getLong("SIZE"))
	   .as("Returns the Long value of the field")
	   .isEqualTo(32);
	   
   }
   
   @Test
   public void  testGetModifiers() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
	   Field field = Integer.class.getField("SIZE");
	   assertThat(Modifier.toString(field.getModifiers()))
	   .as("Returns the Modifier for the field")
	   .isEqualTo("public static final");
   }
   
   @Test
   public void testGetName()  throws NoSuchFieldException {
	   Field field = Integer.class.getField("MIN_VALUE");
	   assertThat(field.getName())
	   .as("Returns the name of the field here")
	   .isEqualTo("MIN_VALUE");
	   
   }
   
   @Test
   public void testGetType() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
	   Field field = Integer.class.getField("MIN_VALUE");
	   assertThat(field.getType().getName())
	   .as("Should return a Class object that identifies the declared type ")
	   .isEqualTo("int");
   }
  
   @Test
   public void testGetHashCode() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
			 Field[] fields = Month.class.getFields();
			 assertThat(fields[0].hashCode())
			 .as("Returns the hashcode of the first value of the field")
			 .isEqualTo(-297508095);
   }
   
   @Test
   public void testisEnumConstant() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException {
			 Field[] fields = Month.class.getFields();
			 assertThat(fields[0].isEnumConstant())
			 .as("Returns boolean true if enum is constant")
			 .isEqualTo(true);
   }
   
   @Test
   public void testgetInt() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Field field = Integer.class.getField("MIN_VALUE");
	   assertThat(field.getInt(field))
	   .as("Returns the int value of te given field")
	   .isEqualTo(-2147483648);
	   
	}
   
   @Test
   public void testgetBoolean() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("isZero");
	   assertThat(field.getBoolean(field))
	   .as("Returns boolean value stored in te field")
	   .isEqualTo(false);
   }
   
   @Test
   public void testgetChar() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("core");
	   assertThat(field.getChar(field))
	  .as("Returns char value stored in te field")
	   .isEqualTo('A');
   }
 
   @Test
   public void testgetShort() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("unique");
	   assertThat(field.getShort(field))
	  .as("Returns short value stored in te field")
	   .isEqualTo((short)239);
   
   }
   
   @Test
   public void testgetAnnoatationByType()  throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("string");
	   Annotation[] annotations = field.getAnnotationsByType(Annotation.class);
	   assertThat(Arrays.deepToString(annotations))
	   .as("Returns Annotation type of the field")
	   .isNotNull()
	   .isEqualTo("[]");
	   
   }
   @Test
   public void testgetAnnoatation()  throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("string");
	   assertThat(field.getAnnotation(Annotation.class))
	   .isNull();
   }
   
   @Test
   public void testisSynthetic()  throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("string");
	   assertThat(field.isSynthetic())
	   .isFalse();
	   
   }
   
   @Test
   public void testsetBoolean() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("isZero");
	   field.setBoolean(field, true);
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.isZero)
	   .isTrue();
   }
   
   @Test
   public void testsetObj() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("value");
	   field.set(field, (short)36);
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.value)
	   .isEqualTo((short)36);
}

   @Test
   public void testsetint() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("work");
	   field.setInt(field, 12345);;
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.work)
	   .isEqualTo(12345);
}
   
   @Test
   public void testsetChar() throws NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("core");
	   field.setChar(field, 'S');
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.core)
	  .isEqualTo('S');
 }
  
   @Test
  public void testsetFloat() throws NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	  Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	  Field field = c.getDeclaredField("war");
	  field.setFloat(field,(float) 12.5);
	  assertThat(community.bodhi.avabodha.java.lang.FieldClass.war)
	  .isEqualTo(12.5f);
	}
  
   @Test 
   public void testsetDouble() throws  NoSuchFieldException, 
   SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
		  Field field = c.getDeclaredField("value");
		  field.setDouble(field, 35.5);
		  assertThat(community.bodhi.avabodha.java.lang.FieldClass.value)
		  .isEqualTo(35.5);
	   
   }
   
  @Test
  public void testsetLong() throws  NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("set");
	   field.setLong(field, 219);
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.set)
	   .isEqualTo(219);
	   
  }
  
  @Test
  public void testsetShort() throws  NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("unique");
	   field.setShort(field,(short)124);
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.unique)
	   .isEqualTo((short)124);
}
  
  @Test
  public void testsetByte() throws  NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	   Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("code");
	   field.setByte(field, (byte)45);
	   assertThat(community.bodhi.avabodha.java.lang.FieldClass.code)
	   .isEqualTo((byte)45);

}
  
  @Test
  public void testtoGenricString() throws NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	  Field[] fields = Month.class.getDeclaredFields();
	  assertThat(fields[1].toGenericString())
	  .as("Return the string which represents this Field")
	  .isEqualTo("public static final java.time.Month java.time.Month.FEBRUARY");

  }
  
  @Test
  public void testsetAccessible() throws NoSuchFieldException, 
  SecurityException, IllegalArgumentException, IllegalAccessException,ClassNotFoundException{
	  Class<?> c = Class.forName("community.bodhi.avabodha.java.lang.FieldClass");
	   Field field = c.getDeclaredField("code");
	   field.setAccessible(true);
	   assertThat(field.isAccessible())
	   .isEqualTo(true);
	   
	   
  
}}

			
			
